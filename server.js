// TODO: Add license information
'use strict';

// load environment configuration
require('dotenv').config(); 

// load mandatory dependencies
const express = require('express');
const fetch = require('node-fetch');
const redirectToHTTPS = require('express-http-to-https').redirectToHTTPS;

// global variables
let ASP_REGISTER = null; 
let PRIVATE_REGISTER = new Map(); 

/**
 * 
 */
function initAspRegister() {
  if (ASP_REGISTER === null) {
    let data = require("./asp-register.json");
    ASP_REGISTER = new Map();

    for (let drug of data.aspRegister) {
      ASP_REGISTER.set(drug.Zulassungsnummer, drug); 
    }
  }

  return ASP_REGISTER; 
}

/**
 * Fetches a drug object from the ASP register. The drug is identified by its
 * license number. The license number is mandatory and found in the drug's 
 * package insert.
 * 
 * The drug object is returned in the response as a JSON object. If the drug 
 * cannot be found the server responds with a HTTP 400 including a dedicated 
 * error-code and description.
 * 
 * @param {*} req Request object
 * @param {*} resp Response object.
 */
function findDrug(req, resp) {
  const license = req.params.licensenumber; 

  if (license === undefined || !ASP_REGISTER.has(license)) {
    resp.status(400).json({
      error_code: "DRUG_NOT_FOUND", 
      error_description: `Drug with license number ${license} not found in ASP register.`
    }); 
    return; 
  }

  // fetch the drug from the asp register
  const drug = ASP_REGISTER.get(license); 

  resp.status(200).json(drug); 
}

/**
 * TODO: Add documentation
 * @param {*} req 
 * @param {*} resp 
 */
function getRegister(req, resp) {
  if (PRIVATE_REGISTER !== undefined) {
    resp.status(200).json(Array.from(PRIVATE_REGISTER.values())); 
    return; 
  }
  
  resp.status(500).json("Private register not available."); 

  // const location = req.params.location || '40.7720232,-73.9732319';
  // const url = `${BASE_URL}/${API_KEY}/${location}`;
  // fetch(url).then((resp) => {
  //   if (resp.status !== 200) {
  //     throw new Error(resp.statusText);
  //   }
  //   return resp.json();
  // }).then((data) => {
  //   setTimeout(() => {
  //     resp.json(data);
  //   }, FORECAST_DELAY);
  // }).catch((err) => {
  //   console.error('Dark Sky API Error:', err.message);
  //   resp.json(generateFakeForecast(location));
  // });
}

/**
 * TODO: Add documentation
 * @param {*} req 
 * @param {*} resp 
 */
function addDrugToRegisterToRegister(req, resp) {
  const licenseNumber = req.params.licensenumber; 

  // if the licensnumber exists fetch the drug and store it in the private 
  // register
  if (ASP_REGISTER.has(licenseNumber)) {
    const drug = ASP_REGISTER.get(licenseNumber); 

    if (!PRIVATE_REGISTER.has(licenseNumber)) {
      PRIVATE_REGISTER.set(drug.Zulassungsnummer, drug); 
    }

    // send the drug as response
    resp.status(200).json(drug); 
    return; 
  } 

  resp.status(400).json(`Drug with license number ${drug.Zulassungsnummer} not found.`); 
}

/**
 * //TODO: Add description
 * @param {*} req 
 * @param {*} resp 
 */
function deleteDrugFromRegister(req, resp) {
  const licenseNumber = req.params.licensenumber; 
  //TODO: Add error handling > register undefined
  //TODO: Optimization: if register is empty take the shortcut
  if (PRIVATE_REGISTER.has(licenseNumber)) {
    PRIVATE_REGISTER.delete(licenseNumber); 
    resp.status(204).send(); 
    return; 
  }

  resp.status(400).json(`Drug with license number ${licenseNumber} not found.`); 
}

/**
 * Starts the Express server.
 *
 * @return {ExpressServer} instance of the Express server.
 */
function startServer() {
  const app = express();
  // obtain port from environment config
  const PORT = process.env.PORT; 

  // init ASP register
  initAspRegister(); 

  // Redirect HTTP to HTTPS,
  app.use(redirectToHTTPS([/localhost:(\d{4})/], [], 301));

  // Logging for each request
  app.use((req, resp, next) => {
    // log request
    const now = new Date();
    const time = `${now.toLocaleDateString()} - ${now.toLocaleTimeString()}`;
    const path = `"${req.method} ${req.path}"`;
    const m = `${req.ip} - ${time} - ${path}`;
    // eslint-disable-next-line no-console
    console.log(m);

    next();
  });

  // Handle requests for the data
  app.get('/drug/:licensenumber', findDrug); 
  // app.get('/register/:registerid', getRegister); 
  // app.post('/register/drug/:registerid/:licensenumber', addDrugToRegister); 
  // app.delete('/register/drug/:registerid/:licensenumber', deleteDrugFromRegister); 

  // Handle requests for static files
  app.use(express.static('public'));

  // Start the server 
  return app.listen(PORT, () => {
    // eslint-disable-next-line no-console
    console.log(`Local DevServer Started on port ${PORT}...`);
  });
}

startServer();
